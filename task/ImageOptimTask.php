<?php

class ImageOptimTask extends BuildTask
{

    protected $title = 'Image Optim';
    protected $description = 'Optimize images via remote server.';
    protected $enabled = true;

    private $api_url;

    /**
     * @param SS_HTTPRequest $request
     */
    public function run($request)
    {
        $config = Config::inst();
        $this->api_url = $config->get('ImageOptim', 'api_url') . '?key=' . $config->get('ImageOptim', 'api_key');
        $this->optimize();
    }

    private function optimize()
    {
        if (filter_var($this->api_url, FILTER_VALIDATE_URL)) {
            $images = File::get()->filter(['ClassName' => 'Image']);

            if ($images->count() === 0) {
                echo 'No images in database.';
                return;
            }

            /* @var $image Image */
            foreach ($images as $image) {
                $optimized_image = OptimizedImage::get()->filter(['FileID' => $image->ID]);

                echo $image->Filename . ' ';

                if ($optimized_image->count() === 0) {
                    $file_path = realpath('../' . $image->Filename);

                    // If file exists.
                    if (false !== $file_path) {
                        $image_file = curl_file_create($file_path);

                        $ch = curl_init($this->api_url);

                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, ['image' => $image_file]);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $optimizer_result = curl_exec($ch);
                        curl_close($ch);

                        $original_file_size = filesize($file_path);
                        $optimized_file_size = strlen($optimizer_result);

                        if ($optimized_file_size === 0) {
                            echo 'could not optimized';
                        } elseif ($optimized_file_size < $original_file_size) {
                            $optimized_image_file_handle = fopen($file_path, 'w');
                            fwrite($optimized_image_file_handle, $optimizer_result);
                            fclose($optimized_image_file_handle);

                            $new_optimized_image = OptimizedImage::create();
                            $new_optimized_image->FileID = $image->ID;

                            try {
                                $new_optimized_image->write();
                            } catch (ValidationException $e) {
                            }

                            echo 'optimized (' . number_format(($optimized_file_size / $original_file_size) * 100,
                                    0) . '%)';

                        } else {
                            echo 'optimized image (' . $optimized_file_size . 'B) is not smaller than original (' . $original_file_size . 'B).';
                        }
                    } else {
                        echo 'file not found in storage only in database';
                    }

                } else {
                    echo 'already optimized';
                }
                echo '<br>';
            }
        } else {
            echo 'invalid api url "' . $this->api_url . '"';
        }
    }
}