<?php

class ImageOptim extends DataExtension
{

    public function onBeforeDelete()
    {
        $data_object = $this->owner;
        if ($data_object->ClassName === 'Image') {
            OptimizedImage::get()->filter(['FileID' => $data_object->ID])->first()->delete();
        }
    }

}